'use strict';

const mysql = require('mysql');

const pool = mysql.createPool({
  host: process.env.HOST,
  database: process.env.DB,
  user: process.env.USER,
  password: process.env.PASSWORD,
  port: process.env.PORT
});

function createResponseBody(status, body) {
  return {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    statusCode: status,
    body: JSON.stringify(body, null, 2)
  };
}

exports.create = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let reqObj = {};

  try {
    if (event.body) {
      reqObj = JSON.parse(event.body);
    }
  } catch (err) {
    console.log('jsonerror', err);
    return callback(null, createResponseBody(400, { error: 'Invalid input' }));
  }

  if (
    typeof reqObj.article === 'undefined' ||
    reqObj.article == null ||
    reqObj.article.length > 15
  ) {
    return callback(null, createResponseBody(400, { error: 'Invalid input' }));
  }

  const query = `insert into article (link) values ('${reqObj.article}');`;
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(
        null,
        createResponseBody(503, { error: 'Connection error' })
      );
    } else {
      connection.query(query, function(error, results, fields) {
        if (error) {
          return callback(null, createResponseBody(503, { error: 'Db error' }));
        }
        connection.release();
        return callback(
          null,
          createResponseBody(200, { result: 'article created' })
        );
      });
    }
  });
};

exports.list = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const query = 'select * from article';
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(
        null,
        createResponseBody(503, { error: 'Connection error' })
      );
    } else {
      connection.query(query, function(error, results, fields) {
        if (error) {
          return callback(null, createResponseBody(503, { error: 'Db error' }));
        }
        connection.release();
        return callback(null, createResponseBody(200, { result: results }));
      });
    }
  });
};

exports.get = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const id = event.pathParameters.id;
  const query = `select * from article where id=${id}`;
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(
        null,
        createResponseBody(503, { error: 'Connection error' })
      );
    } else {
      connection.query(query, function(error, results, fields) {
        if (error) {
          return callback(null, createResponseBody(503, { error: 'Db error' }));
        }
        connection.release();
        if (results.length === 0) {
          return callback(
            null,
            createResponseBody(404, { error: 'Item not found' })
          );
        }
        return callback(null, createResponseBody(200, { result: results[0] }));
      });
    }
  });
};

exports.delete = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const id = event.pathParameters.id;
  const query = `delete from article where id=${id}`;
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(
        null,
        createResponseBody(503, { error: 'Connection error' })
      );
    } else {
      connection.query(query, function(error, results, fields) {
        if (error) {
          return callback(null, createResponseBody(503, { error: 'Db error' }));
        }
        connection.release();
        return callback(null, createResponseBody(200, { result: 'deleted' }));
      });
    }
  });
};

exports.update = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const id = event.pathParameters.id;
  let reqObj = {};
  try {
    if (event.body) {
      reqObj = JSON.parse(event.body);
    }
  } catch (err) {
    console.log('jsonerror', err);
    return callback(null, createResponseBody(400, { error: 'Invalid input' }));
  }

  if (
    typeof reqObj.article === 'undefined' ||
    reqObj.article == null ||
    reqObj.article.length > 15
  ) {
    return callback(null, createResponseBody(400, { error: 'Invalid input' }));
  }

  const query = `update article set link='${reqObj.article}' where id=${id};`;
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(
        null,
        createResponseBody(503, { error: 'Connection error' })
      );
    } else {
      connection.query(query, function(error, results, fields) {
        if (error) {
          console.log('dberror', error);
          return callback(null, createResponseBody(503, { error: 'Db error' }));
        }
        connection.release();
        return callback(null, createResponseBody(200, { result: 'updated' }));
      });
    }
  });
};
