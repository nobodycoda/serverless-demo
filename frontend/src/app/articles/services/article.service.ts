import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Article } from '../models/Article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  url = 'https://y7g9hah0t8.execute-api.us-east-1.amazonaws.com/dev/v1/article';
  // url = 'http://localhost:3000/v1/article';

  constructor(private http: HttpClient) {}

  createArticles(article): Observable<any> {
    return this.http.post(this.url, article);
  }

  getArticle(id): Observable<Article> {
    return this.http.get(`${this.url}/${id}`).pipe(map(data => data['result']));
  }

  listArticles(): Observable<Article[]> {
    return this.http.get(this.url).pipe(map(data => data['result']));
  }

  deleteArticles(id): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }

  updateArticles(article, id): Observable<any> {
    return this.http.post(`${this.url}/${id}`, article);
  }
}
