export interface Article {
  id: number;
  link: string;
}
