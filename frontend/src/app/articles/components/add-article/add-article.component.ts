import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

import { ArticleService } from '../../services/article.service';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  @Input() loading: Boolean;
  @Output() createArticle: EventEmitter<any> = new EventEmitter();
  inputForm;
  errorMessage: string;

  constructor(
    private articleservice: ArticleService,
    private formBuilder: FormBuilder
  ) {
    this.inputForm = new FormGroup({
      article: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {}

  onSubmit(article) {
    // this.loading = true;
    this.createArticle.emit(article);
    this.inputForm.reset();
  }
}
