import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
  @Input() deleteModal: boolean;
  @Output() onDelete: EventEmitter<any> = new EventEmitter();
  @Output() toggleDeleteModal: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  closeModal() {
    this.toggleDeleteModal.emit();
  }

  submitModal() {
    this.onDelete.emit();
  }
}
