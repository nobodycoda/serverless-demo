import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/Article';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  articles: Article[];
  loading: boolean = false;
  deleteModal: boolean = false;

  constructor(
    private articleservice: ArticleService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.listArticles();
  }

  listArticles() {
    this.articleservice.listArticles().subscribe(data => {
      this.articles = data;
    });
  }

  deleteArticle(article) {
    this.articles = this.articles.filter(a => a.id != article.id);
    this.articleservice.deleteArticles(article.id).subscribe(
      data => {},
      error => {
        this._snackBar.open('create article failed', 'error', {
          duration: 2000
        });
        this.listArticles();
      }
    );
  }

  createArticle(article) {
    this.loading = true;
    this.articleservice.createArticles(article).subscribe(
      data => {
        this.loading = false;
        this.listArticles();
      },
      error => {
        this.loading = false;
        this._snackBar.open('create article failed', 'error', {
          duration: 2000
        });
        this.listArticles();
      }
    );
  }
}
