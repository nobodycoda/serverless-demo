import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/Article';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  article: Observable<Article>;
  articleValue: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService
  ) {}

  ngOnInit() {
    this.article = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.articleService.getArticle(params.get('id'))
      )
    );
    this.article.subscribe(
      data => {
        this.articleValue = data.link;
      },
      error => {
        this.router.navigate(['**']);
      }
    );
  }
}
