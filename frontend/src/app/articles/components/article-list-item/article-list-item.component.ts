import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Article } from '../../models/Article';
import { ArticleService } from '../../services/article.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-article-list-item',
  templateUrl: './article-list-item.component.html',
  styleUrls: ['./article-list-item.component.css']
})
export class ArticleListItemComponent implements OnInit {
  @Input() deleteModal: boolean;
  @Input() article: Article;
  @Input() index: number;
  @Output() deleteArticle: EventEmitter<any> = new EventEmitter();
  toggleFormBool: boolean = false;
  updateForm;
  updateLoading: boolean = false;

  colorArray = [
    '#4C51BF',
    '#4A5568',
    '#C53030',
    '#6B46C1',
    '#C05621',
    '#B7791F',
    '#2F855A',
    '#2C7A7B',
    '#2B6CB0'
  ];

  constructor(
    private articleservice: ArticleService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.updateForm = new FormGroup({
      id: new FormControl(this.article.id, Validators.required),
      article: new FormControl(this.article.link, Validators.required)
    });
  }

  toggleDeleteModal() {
    this.deleteModal = !this.deleteModal;
  }

  onDelete() {
    this.deleteArticle.emit(this.article);
  }

  toggleForm() {
    this.toggleFormBool = !this.toggleFormBool;
  }

  onSubmit(article) {
    this.updateLoading = true;
    this.toggleForm();
    this.articleservice.updateArticles(article, this.article.id).subscribe(
      data => {
        this.updateLoading = false;
      },
      error => {
        this.updateLoading = false;
        this._snackBar.open('update failed', 'error', {
          duration: 2000
        });
        this.articleservice.listArticles().subscribe();
      }
    );
  }
}
