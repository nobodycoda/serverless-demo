import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NotFoundComponent } from './components/not-found/not-found.component';
import { ArticlesModule } from './articles/articles.module';
import { WeirdModule } from './weird/weird.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [AppComponent, NotFoundComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    ArticlesModule,
    WeirdModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
