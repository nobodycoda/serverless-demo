import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeirdRoutingModule } from './weird-routing.module';
import { SampleComponent } from './components/sample/sample.component';


@NgModule({
  declarations: [SampleComponent],
  imports: [
    CommonModule,
    WeirdRoutingModule
  ]
})
export class WeirdModule { }
